/* ****Documentation****
Traffic Lights for 1 car lane and 1 pedestrian.
Starts with pedestrian on red and car on green.
When you click the button it will go through 4 different phases:
1. Pedestrian red and car amber
2. Pedestrian green and car red and buzzer
3. pedestrian flashes green and car flashes amber and solid red
4. pedestrian red and car green

If you press the button within a cool down phase it will register that you pressed but will not do anything till the cool down phase is over.



   **connections**
      lights
Car_Red to pin 2 and ground
Car_Amber to pin 3 and ground
Car_Green to pin 4 and ground

      RGB Lights
Far left of RGB LED to pin 5 throught resitor
2nd from left of RBG LED to voltage
3rd from left of RGB LED to pin 6 throught resitor
Far Right of RGB LED to pin 13 throught resitor     not necesery

      Other
Button to pin 7 and Ground through resistor on top left side and 5 volt power on top right



*/

const {Board, Led} = require("johnny-five");
var five = require("johnny-five"),board, button;
var board = new five.Board({/*port:"COM4"*/});    // port is necesary when other USB devices are connected
var wait = require("wait-for-stuff");

board.on("ready", function() {
   //initializing lights/buzzer
   const Car_Red = new five.Led(2);
   const Car_Amber = new five.Led(3);
   const Car_Green = new five.Led(4);
   const Button = new five.Button(7);

   const Amber_To_Ped_Green_Delay = 5;
   const Ped_Green_To_Ped_Amber_Delay = 6;
   const Ped_Amber_To_Ped_Red_Delay = 3;
   const MyPiezo = new five.Piezo(8);


   // declering variavbles
   const cool_Down = 9000;  //wait periode after end of cicle before the next one can initialize
   var pressed = false;    //for the button
   var freeze=false;      // insures car lights are green for at least cool_Down miliseconds 


   const RGB = new five.Led.RGB({
    pins: {
      red: 5,
      green: 6,
      blue : 13 //any pin not being used
    },
    isAnode: true
  });



  //functions

   function Switch_To_Amber(){
      Car_Green.off();
      Car_Amber.on();
   };

   function Switch_To_Ped_Green(){
      Car_Amber.off();
      Car_Red.on();
      RGB.color("#00ff00"); //green
      MyPiezo.frequency(587, 800);    // frequence and time in ml
   }

   function Switch_To_Ped_Amber(){
      Car_Amber.blink();
      RGB.blink(100);   // frequence
   }

   function Switch_To_Ped_Red(){
      Car_Amber.stop();
      Car_Amber.off();
      Car_Red.off();
      Car_Green.on();
      RGB.stop();
      RGB.color("#ff0000"); //red
   }

   function Main(){

      Switch_To_Amber();
      wait.for.time(Amber_To_Ped_Green_Delay);
      Switch_To_Ped_Green();
      wait.for.time(Ped_Green_To_Ped_Amber_Delay);
      Switch_To_Ped_Amber();
      wait.for.time(Ped_Amber_To_Ped_Red_Delay);
      Switch_To_Ped_Red();
   }



   //begin

   //starting with car on green and pedestrian on red
   Car_Green.on(); 
   RGB.off();
   RGB.on();
   RGB.color("#ff0000"); //red


   // main code

   Button.on("down",function() {
      while (freeze===true){
         wait.for.time(0.25);  //wait for a quarter of a second and see if it changes
      };
      
      Main();
      freeze=true;

      board.wait(cool_Down, function(){ 
         freeze=false;          // insures car lights are green for at least cool_Down miliseconds 
      });
   });

});